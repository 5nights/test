# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require bootstrap-datepicker
//= require select2-full

createTag = (tag) ->
  id: tag.term
  text: tag.term 

script = () -> 
  eventSelect = $(".js-example-tags").select2({
    createTag: createTag
    tags: true
  });

  eventSelect.on("select2:select", (e) -> 
    console.log("select2:select", e.target.value);
  );

$(document).on('turbolinks:load', script);
