# Resume model
class Resume < ApplicationRecord
  has_and_belongs_to_many :skills
  validates :name, format: { with: /\A[а-яА-Я]+ [а-яА-Я]+ [а-яА-Я]+\z/, message: 'Формат ввода: Фамилия Имя Отчество' }
  validates :email, email_format: { message: 'Формат: email@domain.org' }
  validates :price, presence: true, numericality: true
  validates :phone, presence: true, numericality: true
  scope :full, lambda { |params|
    if params[:skills].present? && params[:full].present? && params[:full] == 'true'
      q = format 'SELECT "resumes"."id" FROM "resumes"
          INNER JOIN "resumes_skills" ON "resumes_skills"."resume_id" = "resumes"."id"
          GROUP BY "resumes"."id"
          HAVING (COUNT(DISTINCT skill_id) = %s)', params[:skills].size

      where("resumes.id in (#{q})")
    end
  }
  scope :position, lambda { |params|
    if params[:position].present?
      q = format 'SELECT "skills_vacancies"."skill_id" as id FROM "vacancies"
            INNER JOIN "skills_vacancies" ON "skills_vacancies"."vacancy_id" = "vacancies"."id"
            WHERE "vacancies"."id" = %s', params[:position].to_i
      joins(:skills).where("skills.id in (#{q})")
    end
  }
  scope :skills, ->(params) { where(skills: { id: params[:skills] }) if params[:skills].present? }
  scope :active, ->(params) { where(status: true) if params[:active].present? }
  default_scope { includes(:skills).order('price DESC') }
end
