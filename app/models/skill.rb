# Skill model
class Skill < ApplicationRecord
  has_and_belongs_to_many :vacancies
  has_and_belongs_to_many :resumes

  def self.tags_list
    all.collect { |s| [s.title, s.id] }
  end
end
