# Vacancy model
class Vacancy < ApplicationRecord
  has_and_belongs_to_many :skills
  validates :email, email_format: { message: 'Формат: email@domain.org' }
  validates :price, presence: true, numericality: true
  validates :phone, presence: true, numericality: true
  validates :title, presence: true
  scope :full, lambda { |params|
    if params[:skills].present? && params[:full].present? && params[:full] == 'true'
      q = format 'SELECT "vacancies"."id" FROM "vacancies"
            INNER JOIN "skills_vacancies" ON "skills_vacancies"."vacancy_id" = "vacancies"."id"
            GROUP BY "vacancies"."id"
            HAVING (COUNT(DISTINCT skill_id) = %s)', params[:skills].size

      where("vacancies.id in (#{q})")
    end
  }
  scope :skills, ->(params) { where(skills: { id: params[:skills] }) if params[:skills].present? }
  scope :valid, ->(params) { where('valid_before > ?', DateTime.now) if params[:valid].present? }
  default_scope { includes(:skills).order('price DESC') }

  def valid_before
    if self[:valid_before].nil?
      self[:valid_before]
    else
      self[:valid_before].strftime('%d-%m-%Y')
    end
  end

  def created_at
    self[:created_at].strftime('%d-%m-%Y')
  end
end
