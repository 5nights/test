# Skill Generator for Vacancy And Resume models
class SkillGenerator
  attr_writer :skill_titles
  attr_writer :skill_ids

  def initialize(skill_titles)
    raise 'Wrong type: Array required' unless skill_titles.is_a?(Array)
    @skill_titles = trim(skill_titles)
    @skill_ids = []
  end

  def generate
    @skill_titles.each { |title| @skill_ids.push(Skill.find_or_create_by(title: title)) }
    @skill_ids.pluck(:id)
  end

  private

  def trim(skill_ids)
    skill_ids.reject! { |title| title == '' }
  end
end
