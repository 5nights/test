# Resume API query builder
class ResumeBuilder < ApiBuilder
  def scope_whitelist
    %w[skills position full active]
  end

  def model
    Resume
  end
end
