# Query builder for API params
class ApiBuilder
  def initialize(params)
    @builder = model
    @params = params
  end

  def scope_whitelist
    raise NotImplementedError, 'Must implemented method scope_whitelist'
  end

  def model
    raise NotImplementedError, 'Must implemented method model'
  end

  def includes(relationships)
    @builder = @builder.includes(relationships)

    self
  end

  def resolve
    build

    if @params[:page].present?
      @builder.paginate(page: @params[:page], per_page: 5)
    else
      @builder.all
    end
  end

  private

  def build
    @params.each do |p|
      if scope_whitelist.include?(p) && model.methods.include?(p.to_sym)
        @builder = @builder.send(p.to_sym, @params)
      end
    end
  end
end
