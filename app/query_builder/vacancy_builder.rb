# Vacancy API query builder
class VacancyBuilder < ApiBuilder
  def scope_whitelist
    %w[skills full valid]
  end

  def model
    Vacancy
  end
end
