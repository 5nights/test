import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import { selectLocationState } from './redux/reducers/RouterState';

import Home from './containers/Home/Home';
import App from './containers/App/App';
import NotFound from './containers/NotFound/NotFound';

import './base/index.css';


const history = syncHistoryWithStore(browserHistory, store, {
  selectLocationState: selectLocationState
});

render(
  <Provider store={store}>
    <Router
      history={history}>

      <Route path='/' component={App}>
        <IndexRoute component={Home}/>

        {/* Not Found Route */}
        <Route path="*" component={NotFound}/>

      </Route>

    </Router>
  </Provider>,
  document.getElementById('app')
);
