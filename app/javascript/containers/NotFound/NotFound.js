import React from 'react';

export default class NotFound extends React.Component {

  render() {
    return (
      <div style={{
        textAlign: 'center',
        verticalAlign: 'center',
        fontSize: '40vw'
      }}>
        404
      </div>
    )
  }
}
