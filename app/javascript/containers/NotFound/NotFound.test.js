import React from 'react';
import { shallow } from 'enzyme';
import NotFound from './NotFound';

it('renders without crashing', () => {

  const wrapper = shallow(<NotFound />);
  const write = 'NotFound';

  expect(wrapper.contains(write)).toEqual(true);
});
