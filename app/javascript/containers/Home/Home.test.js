import React from 'react';
import { shallow } from 'enzyme';
import Home from './Home';

import ChangeEmployer from '../../components/ChangeEmployer/ChangeEmployer';
import Filter from '../../components/Filter/Filter';
import ItemList from '../../components/ItemList/ItemList';

it('renders without crashing', () => {

  const wrapper = shallow(<Home />);
  const changeEmployer = <ChangeEmployer />;
  const filter = <Filter />;
  const itemList = <ItemList />;

  expect(wrapper.contains(changeEmployer, filter, itemList)).toEqual(true);
});
