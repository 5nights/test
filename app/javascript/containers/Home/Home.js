import React from 'react';

import styles from './Home.css';

import ChangeEmployer from '../../components/ChangeEmployer/ChangeEmployer';
import Filter from '../../components/Filter/Filter';
import ItemList from '../../components/ItemList/ItemList';

export default class Home extends React.Component {
  render() {
    return (
        <div className={styles.container}>
          <ChangeEmployer />
          <Filter />
          <ItemList />
        </div>
    )
  }
}
