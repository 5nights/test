import 'isomorphic-fetch';

import setting from './Settings';

export default function main(options) {

  const optionsValid = () => {

    let valid = '';

    for ( let item in options.query) {

      if (options.query[item] !== '' &&  options.query[item] !== null && options.query[item] !== undefined) {

        if (options.query[item].length !== 0) {

          let arrDetect = options.query[item] instanceof Array;

          if (arrDetect) {

            options.query[item].map((data) => {
              valid += `${item}[]=${data}&`;
            });
          } else {
            valid += `${item}=${options.query[item]}&`;
          }

        }
      }
    }

    return valid
  };

  /*******************************************/

  const getRequest = async () => {

    const headers = {'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'};

    const params = optionsValid();

    const question = params ? `?${params}` : '';

    const opt = {
      method: 'get',
      headers: options.headers ? options.headers : headers
    };

    return await fetch(`${setting.apiUrl}${options.url}${question}`, opt)
      .then(response => response.json())
      .catch(e => e)
  };

  /*******************************************/

  const postRequest = async () => {

    const headers = {'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'};

    const body = optionsValid();

    const opt = {
      method: 'post',
      headers: options.headers ? options.headers : headers,
      body: body
    };

    return await fetch(setting.apiUrl + options.url, opt)
      .then(response => response.json())
      .catch(e => e)
  };

  if (options.method === 'POST') {
    return postRequest()
  }

  return getRequest()
}
