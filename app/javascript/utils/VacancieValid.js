export default function(obj) {
  let arr = [];

  obj.map(ob => {
    arr.push({
      value: ob.id,
      label: ob.title
    })
  });

  return arr;
}
