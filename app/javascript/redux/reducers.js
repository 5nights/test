import { combineReducers } from 'redux-loop';
import { Map } from 'immutable';

import RouterReducer from './reducers/RouterState';
import ChangeEmployerState from './reducers/ChangeEmplouerState';
import ResumeState from './reducers/ResumeState';
import VacanciesState from './reducers/VacanciesState';
import AllSkillsState from './reducers/AllSkillsState';
import AllTitleVacancies from './reducers/AllTitleVacncies';
import FiltredState from './reducers/FiltredState';

const reducers = {
  routing: RouterReducer,
  employer: ChangeEmployerState,
  resume: ResumeState,
  vacancies: VacanciesState,
  skills: AllSkillsState,
  titleVacancies: AllTitleVacancies,
  params: FiltredState
};

const initialState = Map();
const getItem = (child, key) => child.get(key);
const setItem = (child, key, value) => child.set(key, value);

export default combineReducers(
  reducers,
  initialState,
  getItem,
  setItem
);
