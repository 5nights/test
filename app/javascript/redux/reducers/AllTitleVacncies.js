import { fromJS, List } from 'immutable';
import { loop, Effects } from 'redux-loop';
import get from '../../utils/GetData';

const initialState = fromJS({
  loading: false,
  title: []
});

export const TITLE_REQUEST = 'titleVacancies/REQUEST';
export const TITLE_RESPONSE = 'titleVacancies/RESPONSE';


export function request() {
  return {
    type: TITLE_REQUEST
  }
}

async function responseData() {

  const options = {
    method: 'GET',
    url: 'vacancies'
  };

  const data = await get(options);

  return {
    type: TITLE_RESPONSE,
    data
  }
}

export default function PlacesReducer(state = initialState, action) {
  switch (action.type) {

    case TITLE_REQUEST:
      return loop(
        state
          .set('loading', true),
        Effects.promise(responseData, action.data)
      );

    case TITLE_RESPONSE:
      return state.withMutations(state => {
        state.set('loading', false)
          .set('title', List(action.data))
      });
    default:
      return state;
  }
}
