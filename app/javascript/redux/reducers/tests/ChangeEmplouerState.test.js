import { fromJS } from 'immutable';
import reducer, {
  EMPLOYER_CHANGE,
  EMPLOYER_GET,
  change,
  getChange
} from '../ChangeEmplouerState';


describe('change state', () => {

  it('should create an action to EMPLOYER_CHANGE', () => {
    const expectedAction = {
      type: EMPLOYER_CHANGE,
      key: 1
    };
    expect(change()).toEqual(expectedAction)
  });

  it('should create an action to EMPLOYER_GET', () => {

    const expectedActions = {
      type: EMPLOYER_GET
    };

    expect(getChange.toEqual(expectedActions));
  });

  it('should return initialState', () => {
    expect(reducer(undefined, {})).toEqual(fromJS(
      {
        change: 1
      }
    ))
  });

});
