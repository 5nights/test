import configureMockStore from 'redux-mock-store'
import { fromJS } from 'immutable';
import thunk from 'redux-thunk';
import nock from 'nock'
import reducer, {
  TITLE_REQUEST,
  TITLE_RESPONSE ,
  request,
  responseData
} from '../AllTitleVacncies';

import settings from '../../../utils/Settings';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('vacancies state', () => {

  afterEach(() => {
    nock.cleanAll()
  });

  it('should create an action to TITLE_REQUEST', () => {
    const expectedAction = {
      type: TITLE_REQUEST,
      filter: {
        skills: 'Навыки'
      }
    };
    expect(request()).toEqual(expectedAction)
  });

  it('should create an action to TITLE_RESPONSE', () => {

    nock(settings.apiUrl)
      .get('vacancies')
      .reply(200, [{
        rows: [{
          title: 'Водитель'
        }]
      }]);

    const expectedActions = [
      {
        type: TITLE_RESPONSE,
        data: [{
          rows: [{
            title: 'Водитель'
          }]
        }]
      }];

    const store = mockStore({ title: [] });

    return store.dispatch(async dispatch => {
      dispatch(await responseData());

      expect(store.getActions()).toEqual(expectedActions)
    })
  });

  it('should return initialState', () => {
    expect(reducer(undefined, {})).toEqual(fromJS(
      {
        loading: false,
        title: []
      }
    ))
  });

  it('should handle TITLE_REQUEST', () => {

    expect(reducer(undefined, request())).toEqual(fromJS({
      loading: true
    }))
  });
});
