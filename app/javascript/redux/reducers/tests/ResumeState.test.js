import configureMockStore from 'redux-mock-store'
import { fromJS } from 'immutable';
import thunk from 'redux-thunk';
import nock from 'nock'
import reducer, {
  RESUME_REQUEST,
  RESUME_RESPONSE,
  request,
  responseData
} from '../ResumeState';

import settings from '../../../utils/Settings';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('resume state', () => {

  afterEach(() => {
    nock.cleanAll()
  });

  it('should create an action to RESUME_REQUEST', () => {
    const expectedAction = {
      type: RESUME_REQUEST,
      filter: {
        skills: 'Навыки',
        full: false
      }
    };
    expect(request()).toEqual(expectedAction)
  });

  it('should create an action to RESUME_RESPONSE', () => {

    nock(settings.apiUrl)
      .get('resumes?skills=Вождение&full=false')
      .reply(200, [{
        name: 'Водитель',
        price: 40000,
        phone: '+79999999999',
        email: 'mail@gmail.com'
      }]);

    const expectedActions = [
      {
        type: RESUME_RESPONSE,
        data: [{
          name: 'Водитель',
          price: 40000,
          phone: '+79999999999',
          email: 'mail@gmail.com'
        }]
      }];

    const store = mockStore({ resume: [] });

    return store.dispatch(async dispatch => {
      dispatch(await responseData());

      expect(store.getActions()).toEqual(expectedActions)
    })
  });

  it('should return initialState', () => {
    expect(reducer(undefined, {})).toEqual(fromJS(
      {
        loading: false,
        resume: []
      }
    ))
  });

  it('should handle RESUME_REQUEST', () => {

    expect(reducer(undefined, request())).toEqual(fromJS({
      loading: true
    }))
  });
});
