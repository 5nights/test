import configureMockStore from 'redux-mock-store'
import { fromJS } from 'immutable';
import thunk from 'redux-thunk';
import nock from 'nock'
import reducer, {
  VACANCIES_REQUEST,
  VACANCIES_RESPONSE ,
  request,
  responseData
} from '../VacanciesState';

import settings from '../../../utils/Settings';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('vacancies state', () => {

  afterEach(() => {
    nock.cleanAll()
  });

  it('should create an action to VACANCIES_REQUEST', () => {
    const expectedAction = {
      type: VACANCIES_REQUEST,
      filter: {
        skills: 'Навыки',
        full: false
      }
    };
    expect(request()).toEqual(expectedAction)
  });

  it('should create an action to VACANCIES_RESPONSE', () => {

    nock(settings.apiUrl)
      .get('vacancies?skills=Вождение&title=Водитель&full=false')
      .reply(200, [{
        title: 'Водитель',
        price: 40000,
        phone: '+79999999999',
        email: 'mail@gmail.com',
        organization: 'yandex'
      }]);

    const expectedActions = [
      {
        type: VACANCIES_RESPONSE,
        data: [{
          title: 'Водитель',
          price: 40000,
          phone: '+79999999999',
          email: 'mail@gmail.com',
          organization: 'yandex'
        }]
      }];

    const store = mockStore({ vacancies: [] });

    return store.dispatch(async dispatch => {
      dispatch(await responseData());

      expect(store.getActions()).toEqual(expectedActions)
    })
  });

  it('should return initialState', () => {
    expect(reducer(undefined, {})).toEqual(fromJS(
      {
        loading: false,
        vacancies: []
      }
    ))
  });

  it('should handle VACANCIES_REQUEST', () => {

    expect(reducer(undefined, request())).toEqual(fromJS({
      loading: true
    }))
  });
});
