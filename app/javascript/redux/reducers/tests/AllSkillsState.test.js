import configureMockStore from 'redux-mock-store'
import { fromJS } from 'immutable';
import thunk from 'redux-thunk';
import nock from 'nock'
import reducer, {
  SKILLS_REQUEST,
  SKILLS_RESPONSE ,
  request,
  responseData
} from '../AllSkillsState';

import settings from '../../../utils/Settings';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('vacancies state', () => {

  afterEach(() => {
    nock.cleanAll()
  });

  it('should create an action to SKILLS_REQUEST', () => {
    const expectedAction = {
      type: SKILLS_REQUEST,
      filter: {
        skills: 'Навыки',
        full: false
      }
    };
    expect(request()).toEqual(expectedAction)
  });

  it('should create an action to SKILLS_RESPONSE', () => {

    nock(settings.apiUrl)
      .get('skills')
      .reply(200, [{
        rows: [{
          title: 'Водитель'
        }]
      }]);

    const expectedActions = [
      {
        type: SKILLS_RESPONSE,
        data: [{
          rows: [{
            title: 'Водитель'
          }]
        }]
      }];

    const store = mockStore({ skills: [] });

    return store.dispatch(async dispatch => {
      dispatch(await responseData());

      expect(store.getActions()).toEqual(expectedActions)
    })
  });

  it('should return initialState', () => {
    expect(reducer(undefined, {})).toEqual(fromJS(
      {
        loading: false,
        skills: []
      }
    ))
  });

  it('should handle SKILLS_REQUEST', () => {

    expect(reducer(undefined, request())).toEqual(fromJS({
      loading: true
    }))
  });
});
