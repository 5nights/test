import { fromJS, List } from 'immutable';
import { loop, Effects } from 'redux-loop';
import get from '../../utils/GetData';

const initialState = fromJS({
  loading: false,
  skills: []
});

export const SKILLS_REQUEST = 'skills/REQUEST';
export const SKILLS_RESPONSE = 'skills/RESPONSE';


export function request() {
  return {
    type: SKILLS_REQUEST
  }
}

async function responseData() {

  const options = {
    method: 'GET',
    url: 'skills'
  };

  const data = await get(options);

  return {
    type: SKILLS_RESPONSE,
    data
  }
}

export default function PlacesReducer(state = initialState, action) {
  switch (action.type) {

    case SKILLS_REQUEST:
      return loop(
        state
          .set('loading', true),
        Effects.promise(responseData, action.data)
      );

    case SKILLS_RESPONSE:
      return state.withMutations(state => {
        state.set('loading', false)
          .set('skills', List(action.data))
      });
    default:
      return state;
  }
}
