import { fromJS, List } from 'immutable';
import { loop, Effects } from 'redux-loop';
import get from '../../utils/GetData';

const initialState = fromJS({
  loading: false,
  resume: []
});

let allVacancies = [];

export const RESUME_REQUEST = 'resume/REQUEST';
export const RESUME_RESPONSE = 'resume/RESPONSE';


export function request(filter) {
  return {
    type: RESUME_REQUEST,
    filter: filter ? filter : {}
  }
}

async function responseData(filter) {

  const skillsArr = filter.skillsChange ?
    filter.skillsChange.map(skills => skills.value) :
    [];

  let id = filter.vacanChange ? filter.vacanChange.value : '';

  const options = {
    method: 'GET',
    url: 'resumes',
    query: {
      valid: true,
      skills: skillsArr,
      position: id,
      full: filter.full,
      page: filter.page
    }
  };

  let reset = filter.reset;

  const data = await get(options);

  return {
    type: RESUME_RESPONSE,
    data,
    reset
  }
}

export default function PlacesReducer(state = initialState, action) {
  switch (action.type) {

    case RESUME_REQUEST:
      return loop(
        state
          .set('loading', true),
          Effects.promise(responseData, action.filter)
      );

    case RESUME_RESPONSE:
      return state.withMutations(state => {
        if ( !action.reset ) {
          action.data.map(element => {
            allVacancies.push(element)
          })
        } else {
          allVacancies = action.data
        }

        state.set('loading', false)
          .set('resume', List(allVacancies))
      });
    default:
      return state;
  }
}
