import { fromJS } from 'immutable';

const initialState = fromJS({
  change: 1
});

export const EMPLOYER_CHANGE = 'employer/CHANGE';
export const EMPLOYER_GET = 'employer/GET';

export function change(key) {
  return {
    type: EMPLOYER_CHANGE,
    key
  }
}

export function getChange() {
  return {
    type: EMPLOYER_GET
  }
}

export default function MenuReducer(state = initialState, action) {
  switch (action.type) {
    case EMPLOYER_CHANGE:
      return state.set('change', action.key);
    case EMPLOYER_GET:
      return state.set('change', state);
    default:
      return state;
  }
}
