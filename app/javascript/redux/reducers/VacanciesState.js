import { fromJS, List } from 'immutable';
import { loop, Effects } from 'redux-loop';
import get from '../../utils/GetData';

const initialState = fromJS({
  loading: false,
  vacancies: []
});

let allVacancies = [];

export const VACANCIES_REQUEST = 'vacancies/REQUEST';
export const VACANCIES_RESPONSE = 'vacancies/RESPONSE';


export function request(filter) {
  return {
    type: VACANCIES_REQUEST,
    filter: filter ? filter : {}
  }
}

export async function responseData(filter) {

  const skillsArr = filter.skillsChange ?
    filter.skillsChange.map(skills => skills.value) :
    [];

  const options = {
    method: 'GET',
    url: 'vacancies',
    query: {
      valid: true,
      skills: skillsArr,
      full: filter.full,
      page: filter.page

    }
  };

  let reset = filter.reset;

  const data = await get(options);

  return {
    type: VACANCIES_RESPONSE,
    data,
    reset
  }
}

export default function PlacesReducer(state = initialState, action) {
  switch (action.type) {

    case VACANCIES_REQUEST:
      return loop(
        state
          .set('loading', true),
        Effects.promise(responseData, action.filter)
      );

    case VACANCIES_RESPONSE:
      return state.withMutations(state => {

        if ( !action.reset ) {
          action.data.map(element => {
            allVacancies.push(element)
          })
        } else {
          allVacancies = action.data
        }

        state.set('loading', false)
          .set('vacancies', List(allVacancies))
      });
    default:
      return state;
  }
}
