import { fromJS } from 'immutable';

const initialState = fromJS({
  params: {
    reset: false,
    page: 1
  }
});

export const FILTERED_SET = 'filtered/SET';

export function set(params) {

  return {
    type: FILTERED_SET,
    params
  }
}

export default function MenuReducer(state = initialState, action) {
  switch (action.type) {
    case FILTERED_SET:
      return state.set('params', action.params);
    default:
      return state;
  }
}
