import { LOCATION_CHANGE } from 'react-router-redux';
import { Map } from 'immutable';

const initialState = new Map({'locationBeforeTransitions': null});

const immutableRouterReducer = (state = initialState, {type, payload}) => {
  if (type === LOCATION_CHANGE) {
    return state.set('locationBeforeTransitions', payload)
  }

  return state;
};

export const selectLocationState = (state = initialState) => {
  return {locationBeforeTransitions: state.get('routing').get('locationBeforeTransitions')}
};

export default immutableRouterReducer;
