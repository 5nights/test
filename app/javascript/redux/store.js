import { createStore, compose, applyMiddleware } from 'redux';
import { install } from 'redux-loop';
import thunk from 'redux-thunk';

import reducers from './reducers';

const enhancer = compose(
  applyMiddleware(thunk),
  install()
);

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  enhancer
);

export default store;
