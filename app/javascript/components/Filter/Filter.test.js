import React from 'react';
import { shallow } from 'enzyme';
import Filter from './Filter';


it('renders without crashing', () => {

  const wrapper = shallow(<Filter />);
  const btn = wrapper.find('.button button');
  let state = () => wrapper.state();

  btn.simulate('click');

  expect(state.skillsResume).toEqual(true);
  expect(state.skillsVacancies).toEqual(true);
  expect(wrapper.contains(wrapper)).toEqual(true);
});
