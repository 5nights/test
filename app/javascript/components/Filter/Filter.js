import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { Checkbox, Button } from 'react-bootstrap';

import styles from './Filter.css';

import vacancieValid from '../../utils/VacancieValid';

import * as ResumeState from '../../redux/reducers/ResumeState';
import * as VacanciesState from '../../redux/reducers/VacanciesState';
import * as AllSkillsState from '../../redux/reducers/AllSkillsState';
import * as AllTitleVacancies from '../../redux/reducers/AllTitleVacncies';
import * as FiltredState from '../../redux/reducers/FiltredState';

class Filter extends React.Component {

  static PropTypes = {
    employer: PropTypes.number.isRequired,
    loadingVacan: PropTypes.bool,
    vacancies: PropTypes.object,
    loadingResume: PropTypes.bool,
    resume: PropTypes.object,
    loadSkills: PropTypes.bool,
    skills: PropTypes.object
  };

  state = {
    full: false,
    reset: true,
    page: 1,
    filtered: 1
  };

  _vacancChange(val) {
    this.setState({
      vacanChange: val
    })
  }

  _skillsChange(val) {
    this.setState({
      skillsChange: val
    })
  }

  _fullChange() {
    this.setState({
      full: !this.state.full
    })
  }

  _filterSubmit() {
    if (this.props.employer == 1) {
      this.props.getVacancies(this.state);

      this.setState({
        filtered: ++this.state.filtered
      }, () => {
        this.props.filterSet(this.state);
      });
    } else {
      this.props.getResume(this.state);

      this.setState({
        filtered: ++this.state.filtered
      }, () => {
        this.props.filterSet(this.state);
      });
    }
  }

  componentDidMount() {
    this.props.getAllTitle();

    this.props.getAllSkills();
  }

  render() {
    const { employer, skills, titleVacancies, loadSkills, loadTitles } = this.props;

    if (loadSkills || loadTitles) return <div />;

    return (
      <div className={classNames('col-sm-3', styles.container)}>
        <h4>Выбрать параметры</h4>

        <div className={styles.selcetCont}>
          {
            employer == 2 ?
              <div>
                <div className={classNames(styles.employer)}>
                  <label className={styles.selectLabel}>Должность</label>
                  <Select
                    name="form-field-name"
                    noResultsText="Результатов не найдено"
                    placeholder="Выберите должности"
                    value={this.state.vacanChange}
                    options={vacancieValid(titleVacancies)}
                    onChange={::this._vacancChange}
                    multi={false}
                  />
                </div>
                <div className={styles.skills}>
                  <label className={styles.selectLabel}>Навыки</label>
                  <Select
                    name="form-field-name"
                    noResultsText="Результатов не найдено"
                    placeholder="Выберите навыки"
                    value={this.state.skillsChange}
                    options={vacancieValid(skills)}
                    onChange={::this._skillsChange}
                    multi={true}
                  />
                </div>
              </div>
              :
              <div className={styles.skills}>
                <label className={styles.selectLabel}>Навыки</label>
                <Select
                  name="form-field-name"
                  noResultsText="Результатов не найдено"
                  placeholder="Выберите навыки"
                  value={this.state.skillsChange}
                  options={vacancieValid(skills)}
                  onChange={::this._skillsChange}
                  multi={true}
                />
              </div>
          }
        </div>

        <div className={styles.fullCoincidence}>
          <Checkbox
            value={this.state.full}
            onChange={::this._fullChange}
          />
          <span className={styles.textCheck}>Полное совпадение</span>
        </div>

        <div className={classNames(styles.button)}>
          <Button
            bsSize="large"
            bsStyle="info"
            onClick={::this._filterSubmit}
          >
            Найти
          </Button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  employer: state.getIn(['employer', 'change']),
  loadingVacan: state.getIn(['vacancies', 'loading']),
  vacancies: state.getIn(['vacancies', 'vacancies']),
  loadingResume: state.getIn(['resume', 'loading']),
  resume: state.getIn(['resume', 'resume']),
  loadSkills: state.getIn(['skills', 'loading']),
  skills: state.getIn(['skills', 'skills']),
  loadTitles: state.getIn(['titleVacancies', 'skills']),
  titleVacancies: state.getIn(['titleVacancies', 'title']),
  params: state.getIn(['params', 'params'])
});

const mapDispatchToProps = dispatch => ({
  getResume(filter) {
    return dispatch(ResumeState.request(filter));
  },
  getVacancies(filter) {
    return dispatch(VacanciesState.request(filter));
  },
  getAllSkills() {
    return dispatch(AllSkillsState.request());
  },
  getAllTitle() {
    return dispatch(AllTitleVacancies.request());
  },
  filterSet(params) {
    return dispatch(FiltredState.set(params))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
