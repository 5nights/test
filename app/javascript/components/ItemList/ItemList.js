import React from 'react';
import styles from './ItemList.css';
import PropTypes from 'prop-types'
import classNames from 'classnames';
import { connect } from 'react-redux';

import * as ResumeState from '../../redux/reducers/ResumeState';
import * as VacanciesState from '../../redux/reducers/VacanciesState';
import * as FilterState from '../../redux/reducers/FiltredState';

import Spinner from '../Spinner/Spinner';

class ItemList extends React.Component {

  static propTypes = {
    employer: PropTypes.number.isRequired,
    loadingVacan: PropTypes.bool,
    vacancies: PropTypes.object,
    loadingResume: PropTypes.bool,
    resume: PropTypes.object,
    params: PropTypes.object
  };

  state = {
    lazy: false,
    page: 1,
    lastPageDetect: false,
    curScrollPosition: 0
  };

  filter = this.props.params;

  vacanciesList = (data) => {
    if (this.props.loadingVacan) {
      return (
        <div className={classNames(styles.spinnerContainer,
          {[styles.spinnerActive]: this.state.lazy && this.props.loadingVacan})}>
          <Spinner/>
        </div>
      )
    }

    if (data.size == 0) {
      return (
        <h1 className={styles.notData}>Записей не найдено</h1>
      )
    }

    return data.map((item, key) => {
      return (
        <div key={key} className={styles.itemVac}>
          <div className={styles.item}>
            <div className={styles.title}>{item.title}</div>
            <div className={styles.organization}>{item.organization}</div>

            <div className={styles.phone}>Телефон:  <a href={`tel:${item.phone}`}> {item.phone} </a></div>
            <div className={styles.email}>Email:  <a href={`mailto:${item.phone}`}>{item.email}</a></div>

            <div className={styles.skillsList}>
              {
                item.skills.length > 0 &&  <span>Необходимые навыки:</span>
              }
              {
                item.skills.length > 0 &&
                  item.skills.map((element, index) => {
                    return (
                      <div key={index} className={styles.skillItem}>
                        {
                          index+1 == item.skills.length ? `${element.title}.` : `${element.title},`
                        }
                      </div>
                    )
                  })
              }
            </div>
          </div>
          <div className={styles.price}>
            {item.price} руб
          </div>
        </div>
      )
    })
  };

  resumeList = (data) => {
    if (this.props.loadingResume) {
      return (
        <div className={classNames(styles.spinnerContainer,
          {[styles.spinnerActive]: this.state.lazy})}>
          <Spinner/>
        </div>
      )
    }

    if (data.size == 0) {
      return (
        <h1 className={styles.notData}>Записей не найдено</h1>
      )
    }

    return data.map((item, key) => {
      return (
        <div key={key} className={styles.itemResume}>
          <div className={styles.item}>
            <div className={styles.name}>{item.name}</div>

            <div className={styles.phone}>Телефон: <a href={`tel:${item.phone}`}> {item.phone} </a></div>
            <div className={styles.email}>
              {
                item.email &&
                <span>Email: <a href={`mailto:${item.phone}`}>{item.email}</a></span>
              }
            </div>

            <div className={styles.skillsList}>
              {
                item.skills.length > 0 &&  <span>Навыки:</span>
              }
              {
                item.skills.length > 0 &&
                item.skills.map((element, index) => {
                  return (
                    <div key={index} className={styles.skillItem}>
                      {
                        index+1 == item.skills.length ? `${element.title}.` : `${element.title},`
                      }
                    </div>
                  )
                })
              }
            </div>

          </div>
          <div className={styles.item}>
            {item.price} руб
          </div>
        </div>
      )
    });
  };

  _lastScrollLogic(e, countElem, curCountElem) {
    this.setState({
      lazy: false,
      lastPageDetect: (curCountElem - countElem) < 5
    }, () => {
      e.target.scrollTop = this.state.curScrollPosition;
    })
  }

  _scrollContainer(e) {

    let heightElem = e.target.clientHeight;
    let maxScrollHeight = e.target.scrollHeight;
    let curOffsetScroll = e.target.scrollTop;
    let countVacancies = this.props.vacancies.size;
    let countResumes = this.props.resume.size;

    if ((maxScrollHeight - curOffsetScroll) === heightElem && !this.state.lastPageDetect) {
      this.setState({
        lazy: true,
        page: ++this.state.page,
        curScrollPosition: maxScrollHeight + curOffsetScroll
      }, () => {
        this.filter = {
          ...this.props.params,
          page: this.state.page
        };

        this.props.setFilter(this.filter);

        if (this.props.employer === 1) {
          this.props.getVacancies(this.filter).then(() => {
            const curCountElem = this.props.vacancies.size;

            this._lastScrollLogic(e, countVacancies, curCountElem)
          })
            .catch(err => console.log(err))
        } else {
          this.props.getResume(this.filter).then(() => {
            const curCountElem = this.props.resume.size;

            this._lastScrollLogic(e, countResumes, curCountElem)
          })
            .catch(err => console.log(err))
        }
      })
    }
  }

  componentDidMount() {
    this.props.getVacancies(this.state);
    this.props.getResume(this.state);

    this.body.addEventListener('scroll', (e) => this._scrollContainer(e))
  }

  componentDidUpdate(prevProps) {
    if (prevProps.params.filtered !== this.props.params.filtered) {
      this.setState({
        lastPageDetect: false,
        page: 1
      }, () => {
        this.props.setFilter({
          ...this.props.params,
          reset: false
        });

        this.body.scrollTop = 0;
      })
    }
  }

  render() {
    let { employer, vacancies, resume }  = this.props;

    return (
      <div
        className={classNames('col-sm-9', styles.container)}
        ref={(r) => this.body = r}
      >
        {
          employer == 1 ? this.vacanciesList(vacancies) : this.resumeList(resume)
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  employer: state.getIn(['employer', 'change']),
  loadingVacan: state.getIn(['vacancies', 'loading']),
  vacancies: state.getIn(['vacancies', 'vacancies']),
  loadingResume: state.getIn(['resume', 'loading']),
  resume: state.getIn(['resume', 'resume']),
  params: state.getIn(['params', 'params'])
});

const mapDispatchToProps = dispatch => ({
  getResume(page) {
    return dispatch(ResumeState.request(page));
  },
  getVacancies(page) {
    return dispatch(VacanciesState.request(page));
  },
  setFilter(params) {
    return dispatch(FilterState.set(params))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemList);
