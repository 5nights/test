import React from 'react';
import { shallow } from 'enzyme';
import ItemList from './ItemList';

it('renders without crashing', () => {

  const wrapper = shallow(<ItemList />);

  expect(wrapper.contains(wrapper)).toEqual(true);
});
