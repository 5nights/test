import React from 'react';
import styles from './Spinner.css';

export default class Spinner extends React.Component {

  render() {
    return (
      <div className={styles.loader} />
    )
  }
}
