import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

import { Tabs, Tab } from 'react-bootstrap';

import * as ChangeEmployerState from '../../redux/reducers/ChangeEmplouerState';

class ChangeEmployer extends React.Component {

  static propTypes = {
    employer: PropTypes.number.isRequired
  };

  state = {
    key: this.props.employer
  };

  _handleSelect(key) {
    this.setState({
      key: key
    }, () => {
      this.props.SetChange(key)
    })
  }

  render() {

    return (
      <Tabs activeKey={this.state.key} onSelect={::this._handleSelect} id="controlled-tab-example">
        <Tab eventKey={1} title='Работа' />
        <Tab eventKey={2} title='Работники' />
      </Tabs>
    )
  }
}

const mapStateToProps = state => ({
  employer: state.getIn(['employer', 'change'])
});
const mapDispatchToProps = dispatch => ({
  SetChange(key) {
    return dispatch(ChangeEmployerState.change(key))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangeEmployer);
