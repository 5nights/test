import React from 'react';
import { shallow } from 'enzyme';
import ChangeEmployer from './ChangeEmployer';


it('renders without crashing', () => {

  const wrapper = shallow(<ChangeEmployer />);

  expect(wrapper.contains(wrapper)).toEqual(true);
});
