# Resume Controller
class ResumesController < AdminController
  before_action :set_resume, only: [:show, :edit, :update, :destroy]
  before_action :new_resume, only: [:create, :new]
  before_action :set_skill_ids, only: [:create, :update]

  # GET /resumes
  def index
    @resumes = Resume.all
  end

  # GET /resumes/1
  def show; end

  # GET /resumes/new
  def new; end

  # GET /resumes/1/edit
  def edit; end

  # POST /resumes
  def create
    if @resume.save
      redirect_to edit_resume_path(@resume.id), notice: 'Резюме было создано.'
    else
      render :new
    end
  end

  # PATCH/PUT /resumes/1
  def update
    if @resume.update(resume_params)
      redirect_to edit_resume_path(@resume.id), notice: 'Резюме было обновлено.'
    else
      render :edit
    end
  end

  # DELETE /resumes/1
  def destroy
    @resume.destroy

    redirect_to resumes_url, notice: 'Резюме было удалено.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_resume
    @resume = Resume.find(params[:id])
  end

  def new_resume
    @resume = Resume.new(resume_params)
  end

  # Find and create skills for vacancy
  def set_skill_ids
    @resume.skill_ids = SkillGenerator.new(params.fetch(:resume, {})[:skill_ids]).generate
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def resume_params
    params.fetch(:resume, {}).permit(:name, :phone, :price, :email, :status, :skill_ids)
  end
end
