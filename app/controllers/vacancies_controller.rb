# Vacncoes Controller
class VacanciesController < AdminController
  before_action :set_vacancy, only: [:show, :edit, :update, :destroy]
  before_action :new_vacancy, only: [:create, :new]
  before_action :set_skill_ids, only: [:create, :update]

  # GET /vacancies
  def index
    @vacancies = Vacancy.all
  end

  # GET /vacancies/1
  def show; end

  # GET /vacancies/new
  def new; end

  # GET /vacancies/1/edit
  def edit; end

  # POST /vacancies
  def create
    if @vacancy.save
      redirect_to edit_vacancy_path(@vacancy.id), notice: 'Вакансия была создана.'
    else
      render :new
    end
  end

  # PATCH/PUT /vacancies/1
  def update
    if @vacancy.update(vacancy_params)
      redirect_to edit_vacancy_path(@vacancy.id), notice: 'Вакансия была обновлена.'
    else
      render :edit
    end
  end

  # DELETE /vacancies/1
  def destroy
    @vacancy.destroy

    redirect_to vacancies_url, notice: 'Вакансия была удалена.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_vacancy
    @vacancy = Vacancy.find(params[:id])
  end

  def new_vacancy
    @vacancy = Vacancy.new(vacancy_params)
  end

  # Find and create skills for vacancy
  def set_skill_ids
    @vacancy.skill_ids = SkillGenerator.new(params.fetch(:vacancy, {})[:skill_ids]).generate
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def vacancy_params
    params.fetch(:vacancy, {}).permit(:title, :phone, :price, :email, :organization, :valid_before, :skill_ids)
  end
end
