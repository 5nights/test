module Api
  # Resumes API controller
  class ResumesController < ApplicationController
    def list
      @resumes = ResumeBuilder.new(params).resolve

      render json: @resumes.to_json(include: :skills)
    end
  end
end
