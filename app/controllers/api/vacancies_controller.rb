module Api
  # Vacancies API controller
  class VacanciesController < ApplicationController
    def list
      @vacancies = VacancyBuilder.new(params).resolve

      render json: @vacancies.to_json(include: :skills)
    end
  end
end
