module Api
  # Skills API controller
  class SkillsController < ApplicationController
    def list
      render json: Skill.all
    end
  end
end
