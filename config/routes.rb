Rails.application.routes.draw do
  # Auth users
  devise_for :users, controllers: { sessions: 'auth/sessions' }

  # API for SPA
  namespace :api do
    get 'skills', to: 'skills#list', format: false
    get 'resumes', to: 'resumes#list', format: false
    get 'vacancies', to: 'vacancies#list', format: false
  end

  #  Admin routes
  scope 'admin' do
    get '/', to: 'admin#index', as: 'admin_index', format: false
    resources :vacancies, format: false
    resources :resumes, format: false
  end

  # All roads lead to react SPA
  root to: 'index#index', as: 'home', format: false
  get '*path', to: 'index#index', format: false
end
