/* eslint-disable */
'use strict';

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
const CSS_LOADER = 'style-loader!css-loader?modules=true&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]!postcss-loader';
const extarctCSS = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  use: 'css-loader?module&importLoaders=1&localIdentName=[hash:base64:5]!postcss-loader'
});

const config = {
  context: path.join(__dirname, '/app/javascript/'),

  entry: {
    index: ['babel-polyfill', './index']
  },

  output: {
    path: path.join(__dirname, 'public/js/'),
    filename: "bundle.js",
    publicPath: "/"
  },

  resolve: {
    modules: [path.resolve('node_modules')],
    extensions: ['.web.js', '.js', '.jsx', '.json', '.css']
  },

  devtool: NODE_ENV === 'development' ? 'eval' : false,
  target: "web",

  module: {

    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
        options: {
          configFile: './.eslintrc'
        }
      },
      {
        test: /\.jsx?$/,
        loaders: [
          'react-hot-loader',
          {
            loader: 'babel-loader',
            options: {
              presets: ["es2015", "stage-0", "react"],
              plugins: ["transform-decorators-legacy"]
            }
          }
        ],
        exclude: /node_modules/,
        include: __dirname,
      },
      {
        test: /\.css$/,
        loader: NODE_ENV === 'development' ? CSS_LOADER : extarctCSS,
        exclude: /node_modules/
      },
      {
        test: /\.(woff2?|woff|otf|ttf|eot|svg)$/,
        loader: 'file-loader?name=[path][name].[ext]?[hash:base64:5]',
      },
      {
        test: /\.(mp4|webm)$/,
        loader: 'file-loader?name=[path][name].[ext]'
      },
      {
        test: /\.(png|jpg|gif)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name:'[path][name].[ext]?[hash:base64:5]'
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              progressive: true,
              pngquant: {
                quality: "65-90",
                speen: 4
              },
              mozjpeg: {
                progressive: true,
              },
              gifsicle: {
                interlaced: true,
              },
              optipng: {
                optimizationLevel: 7,
              }
            }
          }]
      }
    ]
  },

  devServer: {
    contentBase: path.join(__dirname, 'public'),
    hot: true,
    inline: true,
    historyApiFallback: true,
    host: 'localhost',
    port: 4200,
    stats: {
      colors: true
    },
    proxy: {
      //'/api': 'http://melnikova.digital-mind.ru'
    },
  },

  plugins: [
    new ExtractTextPlugin({
      filename: '../css/[name].css',
      allChunks: true
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
    })
    // new HtmlWebpackPlugin({
    //   template: 'index.ejs',
    //   inject: 'body',
    //   hash: true
    // })
  ]
};

if (NODE_ENV === 'production') {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false
    })
  )
}

if (NODE_ENV === 'development') {
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin()
  )
}

module.exports = config;

/* eslint-enable */
