# Creating skill table
class CreateSkills < ActiveRecord::Migration[5.1]
  def change
    create_table :skills do |t|
      t.string :title
    end
    add_index :skills, :title, unique: true
  end
end
