# Create resumes table
class CreateResumes < ActiveRecord::Migration[5.1]
  def change
    create_table :resumes do |t|
      t.text :name
      t.string :phone
      t.string :email
      t.integer :price
      t.boolean :status
    end
  end
end
