# Make pivot for Resumes and Skills
class CreateJoinTableResumeSkill < ActiveRecord::Migration[5.1]
  def change
    create_join_table :resumes, :skills do |t|
      t.index [:resume_id, :skill_id]
      t.index [:skill_id, :resume_id]
    end
  end
end
