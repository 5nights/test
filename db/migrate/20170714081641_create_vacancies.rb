# Create vacancies table
class CreateVacancies < ActiveRecord::Migration[5.1]
  def change
    create_table :vacancies do |t|
      t.string :title
      t.datetime :created_at
      t.datetime :valid_before
      t.integer :price
      t.string :phone
      t.string :organization
      t.string :email
    end
  end
end
