require 'rails_helper'

RSpec.describe ResumesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/resumes').to route_to('resumes#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/resumes/new').to route_to('resumes#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/resumes/1').to route_to('resumes#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/resumes/1/edit').to route_to('resumes#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/admin/resumes').to route_to('resumes#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/resumes/1').to route_to('resumes#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/resumes/1').to route_to('resumes#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/resumes/1').to route_to('resumes#destroy', id: '1')
    end
  end
end
