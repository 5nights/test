# This will guess the Vacancy class
FactoryGirl.define do
  factory :vacancy do
    title 'тест'
    email 'test@mail.ru'
    price 1111
    phone '79146123445'
  end
end
