# This will guess the Resume class
FactoryGirl.define do
  factory :resume do
    name 'Тест Тестович тестов'
    email 'test@mail.ru'
    price 1111
    phone '79146123445'
  end
end
