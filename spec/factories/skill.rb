# This will guess the Skill class
FactoryGirl.define do
  factory :skill do
    sequence(:title) { |n| "skill#{n}" }
  end
end
