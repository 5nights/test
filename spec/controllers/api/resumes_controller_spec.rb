require 'rails_helper'

RSpec.describe Api::ResumesController, type: :controller do
  let(:valid_session) { {} }
  describe 'GET #list' do
    it 'returns a success response' do
      get :list, params: {}, session: valid_session
      expect(response).to be_success
    end
  end
end
