require 'rails_helper'

RSpec.describe Resume, type: :model do
  pending 'add some examples to (or delete) #{__FILE__}'

  it 'is valid with valid attributes' do
    expect(create(:resume)).to be_valid
  end

  it 'resume for position' do
    params = { position: 1 }
    resumes = generate_stack
    expect(Resume.position(params)).to include(resumes[1])
  end

  it 'resume with full skills|id equality' do
    params = { full: 'true', skills: [1, 2] }
    resumes = generate_stack
    expect(Resume.skills(params).full(params).first.id).to eq(resumes[0].id)
  end

  it 'resume with full skills|count' do
    params = { full: 'true', skills: [1, 2] }
    generate_stack
    expect(Resume.skills(params).full(params).count).to eq(1)
  end

  it 'resume with skills' do
    params = { skills: [1] }
    resumes = generate_stack
    Resume.skills(params).should include(resumes[0])
  end

  def generate_stack
    first_skill = create(:skill, id: 1)
    second_skill = create(:skill, id: 2)
    create(:vacancy, id: 1, skill_ids: 1)
    [
      create(:resume, skills: [first_skill, second_skill]),
      create(:resume, skills: [first_skill])
    ]
  end
end
