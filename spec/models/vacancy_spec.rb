require 'rails_helper'

RSpec.describe Vacancy, type: :model do
  pending 'add some examples to (or delete) #{__FILE__}'

  it 'is valid with valid attributes' do
    expect(create(:vacancy)).to be_valid
  end

  it 'with full skills|id equality' do
    params = { full: 'true', skills: [1, 2] }
    vacancies = generate_stack
    expect(Vacancy.skills(params).full(params).first.id).to eq(vacancies[0].id)
  end

  it 'with full skills|count' do
    params = { full: 'true', skills: [1, 2] }
    generate_stack
    expect(Vacancy.skills(params).full(params).count).to eq(1)
  end

  it 'with skills' do
    params = { skills: [1] }
    vacancies = generate_stack
    Vacancy.skills(params).should include(vacancies[0])
  end

  def generate_stack
    first_skill = create(:skill, id: 1)
    second_skill = create(:skill, id: 2)
    create(:resume, skills: [first_skill])
    [
      create(:vacancy, id: 1, skills: [first_skill, second_skill]),
      create(:vacancy, id: 2, skills: [first_skill])
    ]
  end
end
