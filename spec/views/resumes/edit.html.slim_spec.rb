require 'rails_helper'

RSpec.describe 'resumes/edit', type: :view do
  before(:each) do
    resume = { name: 'Тест Тестович тестов', email: 'test@mail.ru', price: '1111', phone: '1111' }
    @resume = assign(:resume, Resume.create!(resume))
  end

  it 'renders the edit resume form' do
    render

    assert_select 'form[action=?][method=?]', resume_path(@resume), 'post' do
    end
  end
end
