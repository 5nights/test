require 'rails_helper'

RSpec.describe 'vacancies/edit', type: :view do
  before(:each) do
    vacancy = { title: 'тест', phone: '1234', email: 'test@mail.ru', price: '1123' }
    @vacancy = assign(:vacancy, Vacancy.create!(vacancy))
  end

  it 'renders the edit vacancy form' do
    render

    assert_select 'form[action=?][method=?]', vacancy_path(@vacancy), 'post' do
    end
  end
end
